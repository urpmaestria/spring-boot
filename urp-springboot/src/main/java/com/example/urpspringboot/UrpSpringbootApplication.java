package com.example.urpspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrpSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(UrpSpringbootApplication.class, args);
    }

}
