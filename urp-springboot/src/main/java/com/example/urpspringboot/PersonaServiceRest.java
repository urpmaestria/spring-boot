package com.example.urpspringboot;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PersonaServiceRest {

    private static List<Persona> listaPersonas = new ArrayList<Persona>() {
        {
            add(new Persona("Rosa", "Marfil"));
            add(new Persona("Pepito", "Grillo"));
            add(new Persona("Manuela", "Lago"));
        }
    };

    @RequestMapping(value="/personas", method= RequestMethod.GET)
    public List<Persona>  getPersonas(){
        return listaPersonas;
    }
}

